const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const path = require('path');
const webpack = require('webpack');

const config = require('./config');
const cleanDist = config.get('CLEAN_DIST', false);
const devMode = config.get('NODE_ENV') === 'development';

const clientConfig = {
	name: 'browser',
	mode: devMode ? 'development' : 'production',
	devtool: 'inline-source-map',
	entry: {
		client: './src/index.js'
	},
	watchOptions: {
		ignored: ['/node_modules/', '/dist/']
	},
	output: {
		path: path.join(__dirname, 'dist'),
		filename: '[name].js',
		chunkFilename: `module.[name].[hash].bundle.js`,
		publicPath: '/static/'
	},
	resolve: {
		extensions: ['.mjs', '.js', '.json', '.jsx'],
		alias: {
			'../config': '../src/utils/config.js',
			'../../config': '../../src/utils/config.js',
			'../../../config': '../../../src/utils/config.js',
			'../../../../config': '../../../../src/utils/config.js',
			'../../theme.config$': path.join(__dirname, 'src/style/theme/theme.config'),
			'../../../theme.config$': path.join(__dirname, 'src/style/theme/theme.config')
		}
	},
	optimization: {
		minimize: !devMode,
		minimizer: [
			new UglifyJsPlugin({
				cache: false,
				parallel: true,
				sourceMap: false
			}),
			new OptimizeCSSAssetsPlugin({})
		]
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: JSON.stringify(config.get('NODE_ENV')),
				APP_ENV: JSON.stringify('browser')
			}
		}),
		new MiniCssExtractPlugin({
			// Options similar to the same options in webpackOptions.output
			// both options are optional
			filename: '[name].css',
			chunkFilename: '[id].css'
		}),
		cleanDist
			? new CleanWebpackPlugin(['dist'], {
					exclude: []
				})
			: () => {},
		new CopyWebpackPlugin([{ from: './src/assets/*.jpg', to: './', flatten: true }])
	],
	module: {
		rules: [
			{
				test: /\.less$/,
				use: [
					MiniCssExtractPlugin.loader,
					{
						loader: 'css-loader' // translates CSS into CommonJS
					},
					{
						loader: 'less-loader' // compiles Less to CSS
					}
				]
			},
			{
				test: /\.jsx?$/,
				exclude: /(node_modules|public|server)/,
				use: [
					{
						loader: 'babel-loader',
						options: {
							babelrc: false,
							presets: ['es2015', 'react'],
							plugins: [
								'dynamic-import-webpack',
								'jsx-control-statements',
								'transform-object-assign',
								'transform-class-properties',
								'transform-object-rest-spread',
								'transform-export-extensions'
							]
						}
					}
				]
			},
			{
				test: /\.mjs$/,
				include: /node_modules/,
				type: 'javascript/auto'
			},
			{
				test: /\.(jpe?g|png|gif|svg)$/i,
				use: [
					'file-loader',
					{
						loader: 'image-webpack-loader',
						options: {
							mozjpeg: {
								progressive: true,
								quality: 75
							},
							optipng: {
								enabled: false
							},
							pngquant: {
								quality: '75-90',
								speed: 4
							},
							gifsicle: {
								interlaced: false
							},
							webp: {
								quality: 75
							}
						}
					}
				]
			},
			{
				test: /\.(gql)$/,
				exclude: /node_modules/,
				use: {
					loader: 'gql-loader',
					options: {
						baseDir: path.resolve(`${__dirname}/schema/graphql/client`)
					}
				}
			},
			{
				test: /\.(ttf|eot|woff(2)?)(\?[a-z0-9]+)?$/,
				use: [{ loader: 'file-loader' }]
			},
			{
				test: /\.(ico)$/,
				exclude: /(node_modules|public)/,
				use: [{ loader: 'file-loader?name=[name].[ext]' }]
			},
			{
				test: /\.otf(\?.*)?$/,
				use: 'file-loader?name=/fonts/[name].	[ext]&mimetype=application/font-otf'
			},
			{
				test: /\.json$/,
				use: [{ loader: 'json-loader' }]
			}
		]
	}
};

module.exports = clientConfig;
