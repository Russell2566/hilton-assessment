import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Header from './components/Header';
import router from './routes';

const { any } = PropTypes;

function resolveRoute(location, ctx) {
	return router.resolve({ ...location, ctx });
}

class App extends Component {
	static defaultProps = {
		history: any
	};

	static childContextTypes = {
		history: PropTypes.instanceOf(Object).isRequired
	};

	constructor(props) {
		super(props);

		this.state = {
			active: props.route.pathname,
			pages: {
				[props.route.pathname]: props.route
			}
		};

		props.history.listen(this.onHistoryUpdate.bind(this));
	}

	getChildContext() {
		return {
			history: this.props.history
		};
	}

	async onHistoryUpdate(location) {
		const route = await resolveRoute(location, {});

		this.setState({
			active: route.pathname,
			pages: {
				...this.state.pages,
				[route.pathname]: route
			}
		});
	}

	render() {
		const { components, redirect, params, search } = this.state.pages[this.state.active];
		const bodyProps = { params, search };

		if (redirect) {
			this.props.history.push(redirect, undefined, true);
			return null;
		}

		return (
			<div className="site-container">
				<Header />

				{components.map((BodyComponent, i) => (
					<BodyComponent key={`body-component-${i}`} {...bodyProps} />
				))}
			</div>
		);
	}
}

export default async ({ history }) => {
	const ctx = {};
	const route = await resolveRoute(history.location, ctx);

	return props => <App {...props} {...ctx} history={history} route={route} />;
};
