## Rough Stack Details

- Apollo / GraphQL
- Docker
- KOA
- LESS
- MongoDB 3.6
- NodeJS 10.13.0
- ReactJS
- Semantic-UI
- Webpack

The application's interface is fairly flat. In order to see the dashboard, you can use the navigation dropdown in the top right or you can visit http://localhost/dashboard

## Running Assessment

The assessment runs via docker-compose and will start a 3 server stack [mongo, web & api] and a quick action to restore some mongo data. The docker-compose file will build a docker image [902labs/hilton-assessment] on startup if it doesn't exist. To execute this just run:

```
npm run up
```

The server will be available at: http://localhost:3000

## Stopping

1. ctrl+c to break out of docker
2. `npm stop` to stop containers


## Running Assessment in Dev Mode

You can fire up the stack in development mode with:

```
npm run docker-install
npm run up-dev

# or in detached mode

npm run up-dev -- -d
```


### Helpful Scripts

- `npm run docker-install` -- This scripted version of install will run via docker and is important to run this vs npm install which will install against the host OS. This will also run a Webpack build post install.
- `npm run logs` -- tail web logs
- `npm run logs-api` -- tail api logs
- `npm run logs-mongo` -- tail mongo logs
- `npm run logs-webpack` -- tail Webpack logs


### After Action Notes

Since I knew this was going to sit for a bit before being reviewed I took a few minutes to gut the Webpack hot-reloader. I really didn't like the paradigm it created as well as it's horrible restart time and it's prepotency to leaks memory like a sieve. You can review back one commit to take look at my original submission, but no real code was harmed in this tweak. I've also created a Dockerfile to create a semi-proper image to run better for demo mode.

## Online Demo

https://hiltonassessment.902labs.com

## Demo GIF

![](demo.gif)
