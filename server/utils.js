import path from 'path';
import { ApolloServer } from 'apollo-server-koa';
import { makeExecutableSchema } from 'graphql-tools';
import { readFileSync } from 'fs';

import resolvers from './lib';

/* eslint-disable no-console */
export const logger = {
	debug: console.debug,
	error: console.error,
	info: console.log,
	warn: console.warn
};
/* eslint-enable no-console */

// GraphQL Server Setup
const gqlQueries = readFileSync(path.join(__dirname, '..', 'schema/graphql/queries.gql'), 'utf8');
const gqlTypes = readFileSync(path.join(__dirname, '..', 'schema/graphql/types.gql'), 'utf8');

const schema = makeExecutableSchema({
	typeDefs: [gqlTypes, gqlQueries],
	resolvers,
	tracing: true,
	cacheControl: true
});

function getContext({ ctx }) {
	const { method, url } = ctx;
	return { method, url };
}

export const graphQlServer = new ApolloServer({
	schema,
	context: getContext,
	tracing: true,
	formatError: error => {
		logger.error(`graphql error ${error.message}`);

		if (error.extensions) {
			const {
				code,
				exception: { stacktrace = [] }
			} = error.extensions;
			logger.error(code);
			logger.error(stacktrace.reduce((acc, t) => `${acc}\n${t}`, 'stack trace'));

			return new Error(`graphql error ${error.message}`, stacktrace);
		}

		return new Error(`graphql error ${error.message}`);
	}
});

export default {};
