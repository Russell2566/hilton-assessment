// TODO: Try removing this polifill before MVP
import 'babel-polyfill';

import http from 'http';

import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import koaLogger from 'koa-logger';

import config from '../config';
import { graphQlServer, logger } from './utils';
import { connect as dbConn, getDb } from './db';

async function getServer() {
	const app = new Koa();
	const server = http.createServer(app.callback());

	app.use(koaLogger()).use(bodyParser());

	// Setup graphql server endponts
	graphQlServer.applyMiddleware({ app });

	app.on('error', err => {
		logger.error(err);
	});

	async function listen() {
		const port = config.get('api:port');

		return server.listen(port, () => {
			logger.info(`Listening on ${server.address().port}`);
		});
	}

	async function close() {
		await server.close();
		await app.close();
	}

	return Object.freeze({
		listen,
		close
	});
}

function runServer() {
	return new Promise(async resolve => {
		function closeAndExit(cause) {
			try {
				getDb().close();
			} catch (e) {
				// Ignore inability to close db
			}
			resolve(cause);
		}

		process.once('exit', () => closeAndExit('exit'));
		process.once('SIGINT', () => closeAndExit('SIGINT'));
		process.once('SIGUSR2', () => closeAndExit('SIGUSR2'));

		const webServer = await getServer({});
		webServer.listen();
	});
}

dbConn()
	.then(async () => await runServer())
	.then(exit => {
		logger.warn(`exiting cleanly due to ${exit}`);
		process.exit();
	});
