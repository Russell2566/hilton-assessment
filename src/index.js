// TODO: Try removing this polifill before MVP
import 'babel-polyfill';

import React from 'react';
import { render as renderReact } from 'react-dom';
import ApolloClient from 'apollo-client';
import { ApolloProvider } from 'react-apollo';
import { createHttpLink } from 'apollo-link-http';
import { withClientState } from 'apollo-link-state';
import { ApolloLink } from 'apollo-link';
import { InMemoryCache } from 'apollo-cache-inmemory';

import createHistory from 'history/createBrowserHistory';

import config from '../config';
import getApp from './App';

import '../src/style/index.less';

const apiUrl = config.get('apiUrl');

const history = createHistory();
const apolloCache = new InMemoryCache();
const apolloStateLink = withClientState({
	cache: apolloCache
});

const apolloClient = new ApolloClient({
	cache: apolloCache,
	connectToDevTools: true,
	credentials: 'same-origin',
	link: ApolloLink.from([
		apolloStateLink,
		createHttpLink({
			uri: apiUrl
		})
	])
});

async function renderWebpage() {
	const App = await getApp({ history });

	renderReact(
		<ApolloProvider client={apolloClient}>
			<App />
		</ApolloProvider>,
		global.document.querySelector('#assessment-app')
	);
}

history.listen(renderWebpage);
renderWebpage();
