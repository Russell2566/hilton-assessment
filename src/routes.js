import Router from 'universal-router';

function getRouteProcessor(routeProps) {
	return async ({ components }) => {
		return {
			...routeProps,
			components: components.map(({ default: component }) => component)
		};
	};
}

const routes = [
	{
		path: '',
		components: () => [import('./pages/Landing.jsx')],
		render: getRouteProcessor()
	},
	{
		path: '/dashboard',
		children: [
			{
				path: '',
				components: () => [import('./pages/Dashboard.jsx')],
				render: getRouteProcessor()
			},
			{
				path: '/:confirmationId',
				components: () => [import('./pages/Dashboard.jsx')],
				render: getRouteProcessor()
			}
		]
	}
];

async function resolveRoute({ pathname, search, route, next, ctx }, params) {
	if (!route.render) {
		return next();
	}

	const components = await Promise.all(route.components());
	const routeRendered = await route.render({ components, ...ctx });

	return { pathname, search, params, ...routeRendered };
}

export default new Router(routes, {
	resolveRoute,
	errorHandler: error => {
		throw error;
	}
});
