import React, { Component } from 'react';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';
import { Button, Header, Table, Container } from 'semantic-ui-react';
import { graphql, compose } from 'react-apollo';

import Page from '../components/PageFactory';
import PageHelmet from '../components/PageHelmet';

import hotelsQuery from '../../schema/graphql/client/queries/hotels.gql';
import reservationQuery from '../../schema/graphql/client/queries/reservations.gql';

class LandingPage extends Component {
	static contextTypes = {
		history: PropTypes.instanceOf(Object).isRequired
	};

	constructor(props) {
		super(props);

		this.state = {};

		this.handleFilterExec = this.handleFilterExec.bind(this);
		this.handleFilterClear = this.handleFilterClear.bind(this);
		this.renderReservationRow = this.renderReservationRow.bind(this);
	}

	handleFilterExec(filter) {
		this.context.history.push(`/dashboard/${filter}`);
	}

	handleFilterClear() {
		this.context.history.push('/dashboard');
	}

	renderReservationRow({ confirmationId, name, hotel, arrivalDate, departureDate }) {
		return (
			<Table.Row key={confirmationId} style={{ cursor: 'hand' }} onClick={() => this.handleFilterExec(confirmationId)}>
				<Table.Cell content={name} />
				<Table.Cell content={hotel.name} />
				<Table.Cell content={confirmationId} />
				<Table.Cell content={arrivalDate} />
				<Table.Cell content={departureDate} />
			</Table.Row>
		);
	}

	render() {
		// const isMobile = global.innerWidth <= 475;
		const [firstRsv = {}] = this.props.reservations;
		const [defaultHotel = {}] = this.props.hotels;
		const { selectedHotel = firstRsv.hotel || defaultHotel } = this.state;

		return (
			<div className="landing">
				<PageHelmet backgroundImage={selectedHotel.bg} />

				<Container style={{ marginTop: '200px' }}>
					<Header as="h2" color="teal" textAlign="center">
						Reservations
					</Header>
					<div style={{ maxWidth: '800px', margin: '0 auto' }}>
						<Table celled>
							<Table.Header>
								<Table.Row>
									<Table.HeaderCell>Name</Table.HeaderCell>
									<Table.HeaderCell>Hotel</Table.HeaderCell>
									<Table.HeaderCell>Confirmation</Table.HeaderCell>
									<Table.HeaderCell>Arrival</Table.HeaderCell>
									<Table.HeaderCell>Departure</Table.HeaderCell>
								</Table.Row>
							</Table.Header>
							<Table.Body>{this.props.reservations.map(this.renderReservationRow)}</Table.Body>
						</Table>
						<If condition={this.props.params.confirmationId}>
							<Button onClick={this.handleFilterClear}>Clear Filter</Button>
						</If>
					</div>
				</Container>
			</div>
		);
	}
}

function hotelsTx({ hotelsQuery: { loading, hotels = [] } }) {
	return { hotelsLoding: loading, hotels };
}

function reservationsTx({ reservationsQuery: { loading, reservations = [] } }) {
	return { reservationsLoding: loading, reservations };
}

function reservationOpts({ params: { confirmationId } }) {
	const input = confirmationId ? { confirmationId } : {};

	return {
		fetchPolicy: 'cache-and-network',
		variables: { input }
	};
}

export default compose(
	graphql(gql(hotelsQuery), { name: 'hotelsQuery', props: hotelsTx }),
	graphql(gql(reservationQuery), {
		name: 'reservationsQuery',
		options: reservationOpts,
		props: reservationsTx
	})
)(Page(LandingPage));
