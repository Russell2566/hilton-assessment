import React, { PureComponent } from 'react';
import moment from 'moment';
import { Button, Container, Dropdown, Grid, Icon, Label } from 'semantic-ui-react';

import DatePicker from './DatePicker';

class ReservationPicker extends PureComponent {
	render() {
		const { arrivalDate, departureDate, hotels } = this.props;

		const nights = moment(departureDate).diff(moment(arrivalDate), 'd');

		return (
			<div className="reservation-picker">
				<Container className="reservation-wrapper">
					<Grid centered stackable columns={2}>
						<Grid.Row centered className="reservation-selection" columns={3} textAlign="center" verticalAlign="bottom">
							<Grid.Column textAlign="center">
								<DatePicker
									label="Check in"
									name="arrivalDate"
									value={arrivalDate}
									minDate={moment()}
									onChange={this.props.handleDateChange}
								/>
							</Grid.Column>
							<Grid.Column textAlign="center">
								<DatePicker
									label="Check out"
									name="departureDate"
									value={departureDate}
									minDate={moment(arrivalDate).add(1, 'd')}
									onChange={this.props.handleDateChange}
								/>
							</Grid.Column>
							<Grid.Column textAlign="center">
								<Dropdown
									className="hotel-selector"
									text={this.props.selectedHotel.name || 'loading...'}
									onChange={this.props.handleHotelChange}
									options={hotels.map(({ id, name }) => ({
										key: id,
										text: name,
										value: id,
										selected: id === this.props.selectedHotel.id
									}))}
								/>
								<Button as="div" labelPosition="left" onClick={this.props.handleReservation}>
									<Label as="a" basic pointing="right">
										<Choose>
											<When condition={nights === 1}>1 Night</When>
											<Otherwise>{nights} Nights</Otherwise>
										</Choose>
									</Label>
									<Button>
										Book
										<Icon name="angle double right" />
									</Button>
								</Button>
							</Grid.Column>
						</Grid.Row>
					</Grid>
				</Container>
			</div>
		);
	}
}

export default ReservationPicker;
