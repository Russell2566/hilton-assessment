import React from 'react';
import { Grid, Header, Icon } from 'semantic-ui-react';

function DateDisplay(props) {
	return (
		<Grid centered columns={3} onClick={props.onClick} className="date-display" verticalAlign="middle">
			<Grid.Column className="label" width={10} floated="left" textAlign="left">
				<Header>{props.label}</Header>
			</Grid.Column>
			<Grid.Row className="display">
				<Grid.Column>
					<div className="day">{props.day}</div>
				</Grid.Column>
				<Grid.Column style={{ padding: 'inherit 0' }}>
					<div className="month">{props.month}</div>
					<div className="year">{props.year}</div>
				</Grid.Column>
				<Grid.Column className="label" textAlign="left" verticalAlign="bottom" style={{ paddingLeft: 3 }}>
					<Icon name="angle double down" size="small" />
				</Grid.Column>
			</Grid.Row>
		</Grid>
	);
}

export default DateDisplay;
