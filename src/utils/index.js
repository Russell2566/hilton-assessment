/* eslint-disable no-console */
export const logger = {
	info: console.log,
	error: console.error,
	warn: console.warn
};
/* eslint-enable no-console */

export function canUseDOM() {
	return !!global.window;
}

export function get(object, path, defaultValue) {
	if (!object === undefined || object === null) {
		return defaultValue;
	}

	if (!Array.isArray(path)) {
		throw new Error('get path argument must be an array');
	}

	let index = 0;
	const length = path.length;

	while (object != null && index < length) {
		object = object[path[index++]];
	}

	const result = index && index == length ? object : undefined;

	return result === undefined ? defaultValue : result;
}
