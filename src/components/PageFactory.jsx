import React, { Component } from 'react';
import { Image } from 'semantic-ui-react';

import logo from '../assets/logo.png';

export default function(ChildComponent, ChildProps) {
	return class Page extends Component {
		constructor(props) {
			super(props);
		}

		render() {
			return (
				<div className="b2c-page">
					<div className="page-header">
						<Image className="logo" centered src={logo} />
					</div>

					<ChildComponent {...this.props} {...ChildProps} />
				</div>
			);
		}
	};
}
