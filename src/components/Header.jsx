import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { Dropdown, Menu } from 'semantic-ui-react';

class Header extends PureComponent {
	static contextTypes = {
		history: PropTypes.instanceOf(Object).isRequired
	};

	go(next) {
		this.context.history.push(next);
	}

	render() {
		return (
			<div className="top-menu">
				<Menu attached="top" borderless>
					<Menu.Menu position="right">
						<Dropdown item icon="sitemap" simple>
							<Dropdown.Menu>
								<Dropdown.Item onClick={() => this.go('/')}>Customer Experience</Dropdown.Item>
								<Dropdown.Item onClick={() => this.go('/dashboard')}>Dashboard Experience</Dropdown.Item>
							</Dropdown.Menu>
						</Dropdown>
					</Menu.Menu>
				</Menu>
			</div>
		);
	}
}

export default Header;
