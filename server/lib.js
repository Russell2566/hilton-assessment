import shortid from 'shortid';
import { ObjectId } from 'mongodb';

import getDb from './db';

async function getHotels(query = {}) {
	const hotels = await new Promise(resolve =>
		getDb('hotels')
			.find(query)
			.toArray((e, rs) => resolve(rs))
	);

	return hotels.map(({ _id, name, bg }) => ({
		id: _id.toString(),
		name,
		bg
	}));
}

async function getReservations(query = {}) {
	const reservations = await new Promise(resolve =>
		getDb('reservations')
			.find(query)
			.toArray((e, rs) => resolve(rs))
	);

	return reservations.map(async rsv => {
		const [hotel] = await getHotels({ _id: rsv.hotel });

		return {
			arrivalDate: rsv.arrivalDate,
			confirmationId: rsv.confirmationId,
			departureDate: rsv.departureDate,
			hotel,
			id: rsv._id.toString(),
			name: rsv.name
		};
	});
}

export default {
	Query: {
		hotels: async (root, { input }) => {
			return await getHotels(input);
		},
		reservations: async (root, { input }) => {
			return await getReservations(input);
		}
	},
	Mutation: {
		reservation: async (root, { input }) => {
			const confirmationId = shortid
				.generate()
				.replace(/[^a-zA-Z0-9]+/g, '')
				.toUpperCase();

			const { arrivalDate, departureDate, hotel, name } = input;

			await getDb('reservations').insertOne({
				arrivalDate,
				confirmationId,
				departureDate,
				hotel: ObjectId(hotel),
				name
			});

			return { confirmationId };
		}
	}
};
