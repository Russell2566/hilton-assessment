FROM node:10.13.0-stretch

WORKDIR /src

RUN mkdir -p /src; \
	rm -rf /home/node

COPY ./package.json /src/package.json

RUN cd /src; \
	NODE_ENV=development npm install; \
	chown -R node:node /src/*

COPY ./ /src/

RUN cd /src; \
	NODE_ENV=production npm run build; \
	NODE_ENV=production npm prune; \
	rm -Rf /src/src; \
	chown -R node:node /src/*

EXPOSE 3000

ENTRYPOINT ["/bin/bash"]

USER node
