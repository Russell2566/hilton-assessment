import React, { Component } from 'react';
import gql from 'graphql-tag';
import moment from 'moment';
import { Header, Loader, Container, Message, Button } from 'semantic-ui-react';
import { graphql, compose } from 'react-apollo';

import Page from '../components/PageFactory';
import PageHelmet from '../components/PageHelmet';
import ReservationPicker from '../components/ReservationPicker';
import ReservationValidator from '../components/ReservationValidator';

import hotelsQuery from '../../schema/graphql/client/queries/hotels.gql';
import reservationMutation from '../../schema/graphql/client/mutations/reservation.gql';

const pageDefaults = {
	arrivalDate: moment().format('YYYY-MM-DD'),
	confirmationId: undefined,
	departureDate: moment()
		.add(1, 'd')
		.format('YYYY-MM-DD'),
	selectedHotel: undefined,
	submitting: undefined,
	validate: undefined
};

class LandingPage extends Component {
	constructor(props) {
		super(props);

		this.state = { ...pageDefaults };

		this.handleConfirmation = this.handleConfirmation.bind(this);
		this.handleDateChange = this.handleDateChange.bind(this);
		this.handleHotelChange = this.handleHotelChange.bind(this);
		this.handleReservation = this.handleReservation.bind(this);
		this.handleReservation = this.handleReservation.bind(this);
		this.handleReset = this.handleReset.bind(this);
	}

	handleReset() {
		this.setState({ ...pageDefaults });
	}

	handleReservation() {
		const [defaultHotel = {}] = this.props.hotels;
		const { selectedHotel = defaultHotel } = this.state;
		this.setState({ selectedHotel, validate: true });
	}

	async handleConfirmation({ name }) {
		const { arrivalDate, departureDate, selectedHotel } = this.state;

		this.setState({ submitting: true });

		const input = {
			name,
			arrivalDate,
			departureDate,
			hotel: selectedHotel.id
		};

		this.props.createReservation({ variables: { input } }).then(({ data: { reservation: { confirmationId } } }) => {
			this.setState({ confirmationId });
		});
	}

	handleDateChange({ name, value }) {
		const { arrivalDate, departureDate } = {
			...this.state,
			[name]: value
		};

		const nights = moment(departureDate).diff(moment(arrivalDate), 'd');

		const update = {
			arrivalDate,
			departureDate:
				nights > 0
					? departureDate
					: moment(arrivalDate)
							.add(1, 'd')
							.format('YYYY-MM-DD')
		};

		this.setState(update);
	}

	handleHotelChange(e, { value }) {
		const [defaultHotel = {}] = this.props.hotels;
		const { selectedHotel: { id: selectedId } = defaultHotel } = this.state;

		if (value === selectedId) {
			return;
		}

		this.setState({
			selectedHotel: this.props.hotels.find(({ id }) => id === value)
		});
	}

	render() {
		// const isMobile = global.innerWidth <= 475;
		const [defaultHotel = {}] = this.props.hotels;
		const {
			arrivalDate,
			confirmationId,
			departureDate,
			selectedHotel = defaultHotel,
			submitting,
			validate
		} = this.state;

		return (
			<div className="landing">
				<PageHelmet backgroundImage={selectedHotel.bg} />

				<Choose>
					<When condition={confirmationId}>
						<Container style={{ marginTop: '200px' }}>
							<Header as="h2" color="teal" textAlign="center">
								Reservation Created
							</Header>
							<Message style={{ maxWidth: '500px', margin: '0 auto' }}>
								Your reservation was created. Your confirmation number is {confirmationId}.<br />
								<br />
								<div style={{ textAlign: 'center' }}>
									<Button primary onClick={this.handleReset}>
										Acknowledge
									</Button>
								</div>
							</Message>
						</Container>
					</When>
					<When condition={submitting}>
						<Container style={{ marginTop: '200px' }}>
							<Loader active>
								<Header as="h2" color="teal" textAlign="center">
									Saving Reservation
								</Header>
							</Loader>
						</Container>
					</When>
					<When condition={validate}>
						<ReservationValidator
							arrivalDate={arrivalDate}
							departureDate={departureDate}
							handleCancel={this.handleReset}
							handleConfirmation={this.handleConfirmation}
							handleReservation={this.handleReservation}
							selectedHotel={selectedHotel}
						/>
					</When>
					<Otherwise>
						<ReservationPicker
							arrivalDate={arrivalDate}
							departureDate={departureDate}
							handleDateChange={this.handleDateChange}
							handleHotelChange={this.handleHotelChange}
							handleReservation={this.handleReservation}
							hotels={this.props.hotels}
							selectedHotel={selectedHotel}
						/>
					</Otherwise>
				</Choose>
			</div>
		);
	}
}

function hotelsTx({ hotelsQuery: { loading, hotels = [] } }) {
	return { hotelsLoding: loading, hotels };
}

export default compose(
	graphql(gql(hotelsQuery), { name: 'hotelsQuery', props: hotelsTx }),
	graphql(gql(reservationMutation), { name: 'createReservation' })
)(Page(LandingPage));
