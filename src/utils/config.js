import { get as getUtil } from '../utils';

// eslint-disable-next-line jsx-control-statements/jsx-use-if-tag
export default {
	get(configPath, defaultValue) {
		return getUtil(global.CONFIG, configPath.split(':'), defaultValue);
	}
};
