// TODO: Try removing this polifill before MVP
import 'babel-polyfill';

import fs from 'fs';
import http from 'http';
import path from 'path';

import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import koaLogger from 'koa-logger';
import koaSend from 'koa-send';

import config from '../config';
import { logger } from './utils';

/* eslint-disable-next-line no-useless-escape */
const STATIC_VALIDATION_REGEX = /^([a-zA-Z0-9][a-zA-Z0-9\._]*)\.(css|js|png|jpg|ico|eot|svg|woff2?|ttf)$/;

const globalConfig = JSON.stringify({
	baseUrl: config.get('baseUrl'),
	apiUrl: config.get('apiUrl')
});

function getHtml() {
	return `<!DOCTYPE html>
<html>
	<head>
		<title>Hilton Assessment</title>
		<meta name="viewport" content="width=device-width">
		<link rel="stylesheet" href="/static/client.css" />
		<script type="application/javascript">
			var CONFIG = ${globalConfig};
		</script>
	</head>
	<body>
		<div id="assessment-app"></div>
		<script src="/static/client.js" type="application/javascript"></script>
	</body>
</html>`;
}

async function getServer() {
	const app = new Koa();
	const server = http.createServer(app.callback());

	app
		.use(koaLogger())
		.use(async (ctx, next) => {
			if (!ctx.path.startsWith('/static')) {
				await next();
				return;
			}

			const staticFileName = ctx.path.split('/').pop();
			const filePath = path.resolve(__dirname, '../dist', staticFileName);

			try {
				if (!STATIC_VALIDATION_REGEX.test(staticFileName)) {
					throw new Error('Illegal static filename');
				}

				fs.accessSync(filePath, fs.constants.R_OK);
			} catch (e) {
				logger.debug(`static file 404 thrown: ${e}`);

				ctx.status = 404;
				ctx.body = '404';
				return;
			}

			await koaSend(ctx, staticFileName, { root: path.resolve(__dirname, '../dist') });
		})
		.use(bodyParser())
		.use(async ctx => {
			ctx.status = 200;
			ctx.body = getHtml();
		});

	app.on('error', err => {
		logger.error(err);
	});

	async function listen() {
		const port = config.get('web:port');

		return server.listen(port, () => {
			logger.info(`Listening on http://localhost:${server.address().port}`);
		});
	}

	async function close() {
		await server.close();
		await app.close();
	}

	return Object.freeze({
		listen,
		close
	});
}

function runServer() {
	return new Promise(async resolve => {
		process.once('exit', () => resolve('exit'));
		process.once('SIGINT', () => resolve('SIGINT'));
		process.once('SIGUSR2', () => resolve('SIGUSR2'));

		const webServer = await getServer();
		webServer.listen();
	});
}

runServer().then(exit => {
	logger.warn(`exiting cleanly due to ${exit}`);
	process.exit();
});
