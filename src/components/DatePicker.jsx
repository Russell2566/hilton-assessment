import React, { PureComponent, Fragment } from 'react';
import moment from 'moment';
import { Popup } from 'semantic-ui-react';
import { DateInput } from 'semantic-ui-calendar-react';

import DateDisplay from './DateDisplay';

const MOBILE_POP_STYLE = {
	position: 'fixed',
	top: '40%',
	left: '50%',
	bottom: 'auto',
	right: 'auto',
	transform: 'translate(-50%, -50%)'
};

class DatePicker extends PureComponent {
	constructor(props) {
		super(props);

		this.state = {
			open: false
		};

		this.toggle = this.toggle.bind(this);
		this.handleDateChange = this.handleDateChange.bind(this);
	}

	handleDateChange(e, { value }) {
		const { name } = this.props;
		this.setState({ open: false, value });

		if (this.props.onChange) {
			this.props.onChange({ value, name });
		}
	}

	toggle() {
		this.setState({ open: !this.state.open });
	}

	render() {
		const isMobile = global.innerWidth <= 475;
		const dt = moment(this.props.value);

		return (
			<Fragment>
				<Popup
					open={this.state.open}
					basic={isMobile}
					style={isMobile ? MOBILE_POP_STYLE : {}}
					trigger={
						<DateDisplay
							label={this.props.label}
							onClick={this.toggle}
							day={dt.format('DD')}
							month={dt.format('MM')}
							year={dt.format('YYYY')}
						/>
					}
				>
					<DateInput
						inline
						name={this.props.name}
						placeholder="Date"
						value={this.props.value}
						iconPosition="left"
						position="bottom left"
						onChange={this.handleDateChange}
						dateFormat="YYYY-MM-DD"
						minDate={this.props.minDate}
					/>
				</Popup>
			</Fragment>
		);
	}
}

export default DatePicker;
