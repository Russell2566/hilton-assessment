import React from 'react';
import { Helmet } from 'react-helmet';

export default function({ backgroundImage }) {
	return (
		<Helmet>
			<style type="text/css">{`
				body {
					background-color: black;
				}
			`}</style>

			<If condition={backgroundImage}>
				<style type="text/css">{`
					.site-container::before {
						background-image: url('/static/${backgroundImage}');
					}
				`}</style>
			</If>
		</Helmet>
	);
}
