import React, { Component } from 'react';
import moment from 'moment';
import { Button, Grid, Input, Message, Header, Segment, Form } from 'semantic-ui-react';

function prettyDate(d) {
	return moment(d).format('MMM Do YYYY');
}

class ReservationValidator extends Component {
	constructor(props) {
		super(props);

		this.state = {
			fullName: ''
		};

		this.handleConfirmation = this.handleConfirmation.bind(this);
		this.handleNameUpdate = this.handleNameUpdate.bind(this);
	}

	handleNameUpdate(e, { value }) {
		if (this.state.fullName != value) {
			this.setState({ fullName: value });
		}
	}

	handleConfirmation() {
		this.props.handleConfirmation({ name: this.state.fullName });
	}

	render() {
		const { arrivalDate, departureDate, selectedHotel } = this.props;

		const nights = moment(departureDate).diff(moment(arrivalDate), 'd');

		return (
			<div className="validate-reservation">
				<Grid textAlign="center">
					<Grid.Column style={{ maxWidth: 450 }}>
						<Header as="h2" color="teal" textAlign="center">
							Complete &amp; Validate Reservation
						</Header>
						<Form size="large">
							<Segment stacked>
								<Form.Field
									fluid
									icon="user"
									control={Input}
									iconPosition="left"
									placeholder="Full Name"
									label="Your Full Name"
									value={this.state.fullName}
									onChange={this.handleNameUpdate}
								/>

								<Message>
									Staying at {selectedHotel.name} for {nights} nights. Arriving
									{prettyDate(arrivalDate)} and departing {prettyDate(departureDate)}.
								</Message>

								<Button
									fluid
									color="grey"
									size="large"
									disabled={!this.state.fullName}
									onClick={this.handleConfirmation}
								>
									Confirm Reservation
								</Button>
							</Segment>
						</Form>
						<Message>
							Change your mind?{' '}
							<a href="#" onClick={this.props.handleCancel}>
								cancel
							</a>
						</Message>
					</Grid.Column>
				</Grid>
			</div>
		);
	}
}

export default ReservationValidator;
