module.exports = {
	root: true,
	parser: 'babel-eslint',
	ecmaFeatures: {
		'jsx': true
	},
	extends: [
    	'eslint:recommended',
		'plugin:prettier/recommended',
		'plugin:jsx-control-statements/recommended'
	],
	plugins: [
    	'react',
		'jsx-control-statements'
	],
	env: {
		es6: true,
		mocha: true,
		node: true
	},
	rules: {
		'no-console': 1,
		'react/prop-types': 0,
		"react/jsx-uses-react": "error",
		"react/jsx-uses-vars":
		"error"
	}
};
