import { MongoClient } from 'mongodb';

import config from '../config';

const mongoDb = config.get('mongo:db');
const mongoUri = config.get('mongo:uri');

let connection = null;

export function connect() {
	return new Promise((resolve, reject) => {
		MongoClient.connect(
			mongoUri,
			function(err, db) {
				if (err) {
					reject(err);
					return;
				}

				connection = db.db(mongoDb);
				resolve(connection);
			}
		);
	});
}

export function getDb() {
	if (!connection) {
		throw new Error('Call connect first!');
	}

	return connection.collection;
}

export default function get(collection) {
	if (!connection) {
		throw new Error('Call connect first!');
	}

	return connection.collection(collection);
}
