'use strict';

var path = require('path');
var nconf = require('nconf');

var Provider = nconf.Provider;

function loadConfig(conf, args, name, env) {
	args.forEach(function addConfigFile(arg, i) {
		conf.use(name + '-' + ('0000' + i).slice(-4), {
			type: 'file',
			file: path.join(arg, env + '.json')
		});
	});
}

function configModule() {
	var conf = new Provider().argv().env('__'),
		env = conf.get('NODE_ENV') || 'development',
		len = arguments.length,
		args = new Array(len);

	// Convert arguments to a normal array so that we can pass it
	// outside of the function without losing optimizations.
	for (var i = 0; i < len; i += 1) {
		args[i] = arguments[i];
	}

	// Load config files
	loadConfig(conf, args, 'local', 'local');
	loadConfig(conf, args, 'env', env);
	loadConfig(conf, args, 'base', 'base');

	return {
		get: conf.get.bind(conf),
		conf: conf
	};
}

module.exports = configModule(__dirname);
